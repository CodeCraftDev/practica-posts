import React, { Fragment } from 'react'
import App from 'next/app'
import Api from 'cc-api-next'
import { CSSReset, theme, ThemeProvider as ChakraThemeProvider } from '@chakra-ui/core'
import Router from 'next/router'
import NProgress from 'nprogress'
import Cookies from 'universal-cookie'
import withApolloClient from '../src/helpers/apollo'

Router.events.on('routeChangeStart', url => {
  NProgress.start()
})
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

global['Cookies'] = new Cookies()

/**
 * Servicio para manejo de apis
 * @type {Api}
 */
global['oauth'] = Api({
  url: process.env.YOUR_API_URL,
  secret: process.env.YOUR_API_SECRET
})

class MyApp extends App {
  render () {
    const { Component, pageProps, apolloClient } = this.props

    return <ChakraThemeProvider theme={theme}>
      <CSSReset/>
      <Fragment>
        {/*<SessionContextProvider>*/}
        {/*  <ApolloProvider client={apolloClient}>*/}
        <Component {...pageProps} />
        {/*</ApolloProvider>*/}
        {/*</SessionContextProvider>*/}
      </Fragment>
    </ChakraThemeProvider>
  }
}

export default withApolloClient(MyApp)
