import React from 'react'
import { Modal as ModalChakra, Heading, ModalBody, ModalCloseButton, ModalContent, ModalHeader, ModalOverlay } from "@chakra-ui/core";

export const Modal = props => {
  const { title, body, isOpen, size, onClose, closeOnOverlayClick, closeOnEsc, isDisabled, center } = props;

  return <ModalChakra
    isCentered = {center}
    closeOnOverlayClick={closeOnOverlayClick}
    closeOnEsc={closeOnEsc}
    isOpen={isOpen}
    onClose={onClose}
    size={size || 'xl'}>
    <ModalOverlay/>
    <ModalContent>
      <ModalHeader>
        <Heading size='sm'>{title}</Heading>
      </ModalHeader>
      <ModalCloseButton isDisabled={isDisabled}/>
      <ModalBody>
        {body}
      </ModalBody>
    </ModalContent>
  </ModalChakra>
};
