import gql from 'graphql-tag'

export const ACCESS_SETUP = gql`
    query S_Access {
        S_Access(key: "setup") {
            name
            url
        }
    }
`

export const ACCESS_PROFILE = gql`
    query S_Access {
        S_Access(key: "profile") {
            name
            url
        }
    }
`

export const ACCESS_MODULES = gql`
    query S_Access {
        S_Access(keys: ["crm","chat","platform","task","database","game-center"]){
            _id
            name
            url
        }
    }
`
