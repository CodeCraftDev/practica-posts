import React from 'react'
import { GlobalNav } from '@atlaskit/navigation-next'
import QuestionCircleIcon from '@atlaskit/icon/glyph/question-circle'
import { NavUserItem } from './NavUserItem'
import { DoTribe } from './DoTribe'
import { AppSwitcher } from './AppSwitcher'

const globalNavPrimaryItems = [
  {
    id: 'pry-item-1',
    component: DoTribe
  },
  {
    id: 'navigation-app-switcher',
    component: AppSwitcher
  },
]
const globalNavSecondaryItems = [
  // {
  //   id: 'navigation-help',
  //   icon: QuestionCircleIcon,
  //   label: 'Help'
  // },
  {
    id: 'navigation-user',
    component: NavUserItem
  }
]
export const Nav = ({hideAppSwitcher}) => {

  if(hideAppSwitcher){
    globalNavPrimaryItems.splice(1, 1);
  }

  return (
    <GlobalNav
      primaryItems={
        globalNavPrimaryItems
      }
      secondaryItems={
        globalNavSecondaryItems
      }/>
  )
}
