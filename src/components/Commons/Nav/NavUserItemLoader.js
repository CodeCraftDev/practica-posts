import React from "react"
import ContentLoader from "react-content-loader"

export const NavUserItemLoader = () => (
  <ContentLoader
    height={32}
    width={32}
    speed={2}
  >
    <circle cx="16" cy="16" r="16" />
  </ContentLoader>
)
