import React, { Component } from 'react'
import {
  BackItem,
  GoToItem,
  MenuSection,
  withNavigationViewController
} from "@atlaskit/navigation-next";


const SectionWrapper = props => (
  <div
    {...props}
  />
);

const RootMenu = props => (
  <GoToItem
    goTo="content"
    text={props.text || 'Item'}
    before={props.before || null}
  />
);

const ContextualMenu = props => (
  <div>
    <BackItem goTo="root" />
    {
      props.options && props.options.map((option, index) => (<div key={index}>{option}</div>))
    }
  </div>
);

const VIEWS = {
  root: RootMenu,
  content: ContextualMenu,
};

const Noop = () => null;

class ViewRegistry extends Component {
  componentDidMount() {
    const { navigationViewController, root = {text:"Sample"} } = this.props;
    ["root", "content"].forEach(viewId => {
      navigationViewController.addView({
        id: viewId,
        type: 'product',
        getItems: () => [],
      });
    });
    navigationViewController.setView('root');
  }

  render() {
    return null;
  }
}

const ProductNavigation = ({
  navigationViewController: {
    state: { activeView },
  },
  primaryOption = {},
  secondaryOptions = []
}) => {
  const CurrentMenu = activeView ? VIEWS[activeView.id] : Noop;
  const id = (activeView && activeView.id) || undefined;
  const parentId =
    activeView && activeView.id === 'content' ? 'root' : undefined;

  return (
    <SectionWrapper>
      <MenuSection id={id} parentId={parentId}>
        {
          () => {
            return activeView && activeView.id === 'root' ? <CurrentMenu {...primaryOption}/> : <CurrentMenu options={secondaryOptions}/>
          }
        }
      </MenuSection>
    </SectionWrapper>
  );
};

export const ConnectedProductNavigation = withNavigationViewController(
  ProductNavigation,
);
export const ConnectedViewRegistry = withNavigationViewController(ViewRegistry);
