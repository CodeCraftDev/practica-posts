import React from 'react';
import { Box } from '@chakra-ui/core'
import { ButtonItem, MenuGroup, Section, HeadingItem, LinkItem } from '@atlaskit/menu';
import { NavUserMenuStyles } from './styles'
import ShortcutIcon from '@atlaskit/icon/glyph/shortcut';

export const NavUserMenu = ({logout, session, setupAccess, profileAccess}) => {

  return (
    <Box
      style={NavUserMenuStyles}
    >
      <MenuGroup>
        <Section>
          <HeadingItem>Platform</HeadingItem>
          {
            setupAccess && (
              <LinkItem
                href={setupAccess.url}
                target={'_blank'}
                elemAfter={<ShortcutIcon size={'small'} />}
              >
                {setupAccess.name}
              </LinkItem>
            )
          }
        </Section>
        <Section hasSeparator>
          <HeadingItem>{session.name} {session.lastName}</HeadingItem>
          {
            profileAccess && (
              <LinkItem
                href={profileAccess.url}
                target={'_blank'}
                elemAfter={<ShortcutIcon size={'small'} />}
              >
               {profileAccess.name}
              </LinkItem>
            )
          }
          <ButtonItem onClick={logout}>Logout</ButtonItem>
        </Section>
      </MenuGroup>
    </Box>
  );
};
