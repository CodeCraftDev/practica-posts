import React, { useEffect, useState } from 'react';
import {
  Box,
  Checkbox,
  Icon,
  IconButton,
  Image,
  Popover, PopoverArrow, PopoverBody,
  PopoverContent,
  PopoverTrigger,
  Text,
  Tooltip,
  Collapse
} from "@chakra-ui/core";
import { Link } from "./../../../../routes";
import phoneNumber from "../../../helpers/phoneNumber";
import moment from 'moment';
import { DataTableLoader} from './DataTableLoader'

export const DataTable = ({ list, actions, leftActions, head, selection, page, limit, selected, setSelected, loading, collapse }) => {
  let colspan = actions ?
    head.length + 1 :
    selection ?
      head.length + 1 :
      head.length;
  colspan = leftActions || collapse ? colspan + 1 : colspan
  const toPaginateList = page ?
    list.slice(limit * (page - 1), limit * page) :
    list;

  const setArray = value => {
    const array = [];

    toPaginateList.map(item => {
      if (item._id) {
        array.push(value);
      }
    });

    return array;
  }

  const [showCollapse, setShowCollapse] = useState('');
  const [checkedItems, setCheckedItems] = useState(setArray(false));
  const allChecked = checkedItems.every(Boolean);
  const isIndeterminate = checkedItems.some(Boolean) && !allChecked;

  const splitItem = (label, item) => {
    const fields = label.split('.');
    switch (fields.length) {
      case 2:
        return item[fields[0]] ? item[fields[0]][fields[1]] : '';
      case 3:
        return item[fields[0]] ? item[fields[0]][fields[1]] ? item[fields[0]][fields[1]][fields[2]] : '' : '';
      default:
        return item[fields[0]];
    }
  };

  const buildParams = (action, item) => {
    let object = {};
    action.params ?
      action.params.map((action, index) => {
        object[action.label] = splitItem(action.key, item);
      }) :
      object = {id: item._id || item.id};
    return object;
  }

  const getValue = (headItem, item) => {
    let value;

    switch (headItem.type) {
      case 'date':
        let v = item[headItem.field];
        /**
         * prueba
         */
        const fields = headItem.field.split('.');
        value = moment(item[fields[0]][fields[1]]).format(headItem.format || 'MM/DD/YYYY');
        break;
      case 'phone':
        value = phoneNumber(item[headItem.field]);
        break;

      case 'array':
        //por si acaso...
        if (!item[headItem.field]) {
          item[headItem.field] = [];
        }

        for (let i = 0; i < item[headItem.field].length; i++) {
          if (i === 0) {
            value = item[headItem.field][i];
          } else {
            value = value + ', ' + item[headItem.field][i];
          }

        }
        break;

      case 'number':
        value = <Box> {item[headItem.field]} </Box>;
        break;

      case 'icon-true/false':
        value = item[headItem.field] === true ?
          <Box d="flex" justifyContent="flex-start"><Icon name="check" size={headItem.size || '32px'}
                                                          color="green.500"/></Box> :
          <Box d="flex" justifyContent="flex-start"><Icon name="close" size={headItem.size || '32px'}
                                                          color="red.500"/></Box>;
        break;

      case 'colorLabel':
        value = <Box color={item[headItem.field]}> {item[headItem.field]} </Box>;
        break;

      case 'color':
        value = <Box w="50%" size="50%" h="25px" bg={item[headItem.field]}/>;
        break;

      case 'img':
        value = <Box> <Image src={item[headItem.field]} atl={item[headItem.field]} size={headItem.size || '32px'}/> </Box>;
        break;

      case 'compositeImg':
        value = <Box h={headItem.size || '32px'} w={headItem.size || '32px'}>
          <Box pos="relative" h="100%" w="100%">

            <Box h="100%" w="100%" overflow="hidden">
              {item[headItem.field].map((img, index) => (
                <Image key={index} pos="absolute" src={img} zIndex={index}
                       size="100%"
                />
              ))}
            </Box>

          </Box>
        </Box>;
        break;

      case 'img+label':
        value = <Box d="flex">
          <Box d="flex"> <Image src={item[headItem.img]} atl={item[headItem.field]} size={headItem.size || '32px'}/> </Box>
          <Box d="flex"> {item[headItem.field]} </Box>
        </Box>;
        break;

      case 'label+func':
        value = <Box d="flex">
          <Box d="flex" onClick={() => headItem.func()}> {item[headItem.field]} </Box>
        </Box>;
        break;

      case 'internal-link':
        value = <Box>
          <Link route={item[headItem.link]}>
            <a>
              {item[headItem.field]}
            </a>
          </Link>
        </Box>;
        break;

      case 'link':
        value = <Box>
          <a href={item[headItem.link]} target="_blank">{item[headItem.field]}</a>
        </Box>;
        break;

      case 'multi':
        value = headItem.subField ?
          <Popover>
            <PopoverTrigger>
              <Box>
                {splitItem(headItem.field, item)}
                <Tooltip label={'view content'}>
                  {splitItem(headItem.subField, item) !== '' ? <IconButton
                    size='xs'
                    style={{marginLeft: '5px', marginBottom: '5px'}}
                    variant='link'
                    aria-label={'action.label'}
                    icon={'view'}/>:<></>}
                </Tooltip>
              </Box>
            </PopoverTrigger>
            <PopoverContent zIndex={4}>
              <PopoverArrow/>
              <PopoverBody>
                <Text my={2} dangerouslySetInnerHTML={{__html: splitItem(headItem.subField, item)}}/>
              </PopoverBody>
            </PopoverContent>
          </Popover> :
          splitItem(headItem.field, item);
        break;

      // case 'linkArray':
      //   //por si acaso...
      //   if(item[headItem.field] === undefined || item[headItem.fieldLinks] === undefined
      //     || item[headItem.fieldLinks].length !== item[headItem.field].length
      //     || item[headItem.field].length !== item[headItem.fieldLinks].length){
      //     item[headItem.field] = [];
      //     headItem.fieldLinks = [];
      //   }
      //
      //   for(let i = 0; i < item[headItem.field].length; i++){
      //     if(i === 0){
      //       value = <Box d="flex" mx="2px">
      //         <Link href={item[headItem.fieldLinks][i]} isExternal>
      //           item[headItem.field][i]
      //         </Link>
      //       </Box>;
      //     }
      //     else{
      //       value = <Box d="flex" mx="5px">
      //         <Link href={item[headItem.fieldLinks][i]} isExternal>
      //           | item[headItem.field][i]
      //         </Link>
      //       </Box>;
      //     }
      //
      //   }
      //   break;

      default:
        // value = toLowerCase(item[headItem.field] || item[headItem]);
        value = item[headItem.field] || item[headItem];
        break;
    }

    return value;

  };

  const changeSelected = id => {
    const idx = selected.indexOf(id);
    if (idx > -1) {
      setSelected(selected => selected.filter((id, i) => i !== idx));
    } else {
      setSelected(selected => [...selected,
        id]);
    }
  };

  const changeSelectedAll = (checked) => {
    toPaginateList.map(item => {
      const idx = selected.indexOf(item._id);
      if (checked) {
        if (idx === -1) {
          setSelected(selected => [...selected,
            item._id]);
        }
      } else {
        setSelected([]);
      }
    });
  };

  const setValueInArray = (value, index) => {
    const valueInArray = checkedItems;
    valueInArray[index] = value;

    return valueInArray;
  }

  useEffect(() => {
    setCheckedItems(setArray(false));
    if (setSelected) {
      setSelected([]);
    }
  }, [page]);

  const renderActions = (actions, item) => {
    return actions.map((action, index) => (
        action.isLink
          ?
          <Link key={index} route={action.isLink.route} params={buildParams(action.isLink, item)}>
            <a>
              <Tooltip label={action.label}>
                <IconButton
                  style={{marginRight: '5px'}}
                  size='xs'
                  variantColor={action.color}
                  variant='outline'
                  aria-label={action.label}
                  icon={action.icon}/>
              </Tooltip>
            </a>
          </Link>
          :
          <Tooltip key={index} label={action.label}>
            <IconButton
              key={index}
              style={{marginRight: '5px'}}
              size='xs'
              variantColor={action.color || 'black'}
              variant='outline'
              aria-label={action.label}
              icon={action.icon}
              onClick={() => action.handler(item)}/>
          </Tooltip>
      )
    )
  }

  const renderCollapse = (Component, data) => (<Component {...data} />)

  const handleCollapse = itemId => {
    if(showCollapse === itemId){
      setShowCollapse('')
    }else{
      setShowCollapse(itemId)
    }
  }

  return <div className="table-responsive-vertical">
    {loading ?
      <DataTableLoader /> :
      <table className="table table-hover table-condensed table-bordered">
        <thead>
        <tr>
          {
            selection && <th className='check'>
              <Checkbox
                size='lg'
                isChecked={allChecked}
                isIndeterminate={isIndeterminate}
                onChange={(e) => {
                  changeSelectedAll(e.target.checked)
                  setCheckedItems(setArray(e.target.checked))
                }}
              />
            </th>
          }
          {
            (leftActions || collapse !== undefined) && <th className='actions'>actions</th>
          }
          {head.map((headItem, index) =>
            <th key={index}>{headItem.label || headItem}</th>
          )}
          {
            actions && <th className='actions'>actions</th>
          }
        </tr>
        </thead>

        {list && toPaginateList.map((item, index) =>
          <tbody key={item._id || item.id}>
            <tr>
              {
                selection && <td className='check'>
                  <Checkbox
                    size='lg'
                    isChecked={checkedItems[index]}
                    onChange={(e) => {
                      changeSelected(item._id || item.id)
                      setCheckedItems(setValueInArray(e.target.checked, index))
                    }}
                  />
                </td>
              }
              {
                (leftActions || collapse !== undefined) && <td className='actions'>
                  {
                    <Tooltip label='Collapse'>
                      <IconButton
                        style={{marginRight: '5px'}}
                        size='xs'
                        variantColor={'black'}
                        variant='outline'
                        aria-label='Collapse'
                        icon='chevron-down'
                        onClick={() => {handleCollapse(item._id || item.id)}}/>
                    </Tooltip>
                  }
                  { leftActions && renderActions(leftActions, item) }</td>
              }
              {head.map((headItem, index) => (
                <td key={index}>
                  <Box {...headItem.options} style={headItem.style}>{getValue(headItem, item)}</Box>
                </td>
              ))}
              {
                actions && <td className='actions'> { renderActions(actions, item) }</td>
              }
            </tr>
            {
              collapse && <tr className='tr-no-style'>
                <td colSpan={colspan} className='td-no-style'>
                  <Collapse isOpen={showCollapse === (item._id || item.id)}>
                    {renderCollapse(collapse, item)}
                  </Collapse>
                </td>
              </tr>
            }
          </tbody>
        )}

      </table>
    }
    {/*language=css*/}
    <style jsx>{`
       table {
        border-spacing: 0 !important;
        border: solid 1px #E2E8F0 !important;
        border-radius: 0.25rem !important;
      }

      .table {
        width: 100%;
        max-width: 100%;
        /*margin-bottom: 2rem;*/
        background-color: #ffffff;
        border-radius: 0.25rem !important;
        /*-webkit-box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);*/
        /*box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);*/
      }

      .shadow-z-1 {
        -webkit-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .12),
        0 1px 2px 0 rgba(0, 0, 0, .24);
        -moz-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .12),
        0 1px 2px 0 rgba(0, 0, 0, .24);
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .12),
        0 1px 2px 0 rgba(0, 0, 0, .24);
      }

      .table > thead > tr,
      .table > tbody > tr,
      .table > tfoot > tr {
        -webkit-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        transition: all 0.3s ease;
      }

      .table > thead > tr > th,
      .table > tbody > tr > th,
      .table > tfoot > tr > th,
      .table > thead > tr > td,
      .table > tbody > tr > td,
      .table > tfoot > tr > td {
        text-align: left;
        padding: 1.6rem;
        vertical-align: top;
        border-top: 0;
        -webkit-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        transition: all 0.3s ease;
        text-overflow: ellipsis;
        white-space:nowrap;
        overflow:hidden;
        max-width: calc(100vh / ${head.length});
        text-transform: capitalize !important;
      }

      .table > thead > tr > th {
        font-weight: 400;
        color: #757575;
        vertical-align: bottom;
        border-bottom: 1px solid rgba(0, 0, 0, 0.12);
      }

      .table > caption + thead > tr:first-child > th,
      .table > colgroup + thead > tr:first-child > th,
      .table > thead:first-child > tr:first-child > th,
      .table > caption + thead > tr:first-child > td,
      .table > colgroup + thead > tr:first-child > td,
      .table > thead:first-child > tr:first-child > td {
        border-top: 0;
      }

      .table > tbody + tbody {
        border-top: 1px solid rgba(0, 0, 0, 0.12);
      }

      .table .table {
        background-color: #ffffff;
      }

      .table .no-border {
        border: 0;
      }

      .table-condensed > thead > tr > th,
      .table-condensed > tbody > tr > th,
      .table-condensed > tfoot > tr > th,
      .table-condensed > thead > tr > td,
      .table-condensed > tbody > tr > td,
      .table-condensed > tfoot > tr > td {
        padding: 0.8rem;
      }

      .table-bordered {
        border: 0;
      }

      .table-bordered > thead > tr > th,
      .table-bordered > tbody > tr > th,
      .table-bordered > tfoot > tr > th,
      .table-bordered > thead > tr > td,
      .table-bordered > tbody > tr > td,
      .table-bordered > tfoot > tr > td {
        border: 0;
        border-bottom: 1px solid #e0e0e0;
      }

      .table-bordered > thead > tr > th,
      .table-bordered > thead > tr > td {
        border-bottom-width: 1px;
      }

      .table-striped > tbody > tr:nth-child(odd) > td,
      .table-striped > tbody > tr:nth-child(odd) > th {
        background-color: #f5f5f5;
      }

      .table-hover > tbody > tr:hover > td,
      .table-hover > tbody > tr:hover > th {
        background-color: rgba(221, 221, 221, 0.2);
        cursor: pointer;
      }

      .actions {
        padding: 11px 5px !important;
        min-width: 100px;
      }

      .check {
        width: 49px;
        /*padding: 8px 25px !important;*/
        /*display: grid;*/
        /*justify-items: center;*/
        /*align-items: center;*/
      }
      
      .data-not-found {
        height: calc(100vh - 200px);
        justify-items: center;
        text-align: center !important;
        padding-top: 30% !important;
      }
      
      table .tr-no-style td {
        border-color: transparent !important;
      }
      table tbody td.td-no-style {
        padding: 0 !important;
        margin: 0 !important;
        background-color: #eee !important;
       
      }

      @media screen and (max-width: 768px) {
        .table-responsive-vertical > .table {
          margin-bottom: 0;
          background-color: transparent;
        }

        .table-responsive-vertical > .table > thead,
        .table-responsive-vertical > .table > tfoot {
          display: none;
        }

        .table-responsive-vertical > .table > tbody {
          display: block;
        }

        .table-responsive-vertical > .table > tbody > tr {
          display: block;
          border: 1px solid #e0e0e0;
          border-radius: 2px;
          margin-bottom: 1.6rem;
        }

        .table-responsive-vertical > .table > tbody > tr > td {
          background-color: #ffffff;
          display: block;
          vertical-align: middle;
          text-align: right;
          max-width: 100% !important;
        }

        .table-responsive-vertical > .table > tbody > tr > td[data-title]:before {
          content: attr(data-title);
          float: left;
          font-size: inherit;
          font-weight: 400;
          color: #757575;
        }

        .table-responsive-vertical.shadow-z-1 {
          -webkit-box-shadow: none;
          -moz-box-shadow: none;
          box-shadow: none;
        }

        .table-responsive-vertical.shadow-z-1 > .table > tbody > tr {
          border: none;
          -webkit-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);
          -moz-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);
          box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);
        }

        .table-responsive-vertical > .table-bordered {
          border: 0;
        }

        .table-responsive-vertical > .table-bordered > tbody > tr > td {
          border: 0;
          border-bottom: 1px solid #e0e0e0;
        }

        .table-responsive-vertical > .table-bordered > tbody > tr > td:last-child {
          border-bottom: 0;
        }

        .table-responsive-vertical > .table-striped > tbody > tr > td,
        .table-responsive-vertical > .table-striped > tbody > tr:nth-child(odd) {
          background-color: #ffffff;
        }

        .table-responsive-vertical > .table-striped > tbody > tr > td:nth-child(odd) {
          background-color: #f5f5f5;
        }

        .table-responsive-vertical > .table-hover > tbody > tr:hover > td,
        .table-responsive-vertical > .table-hover > tbody > tr:hover {
          background-color: #ffffff;
        }

        .table-responsive-vertical > .table-hover > tbody > tr > td:hover {
          background-color: rgba(0, 0, 0, 0.12);
        }


        .table-responsive-vertical > .table-hover > tbody > tr > td.check {
          display: none;
        }
      }

    `}</style>
  </div>
};
