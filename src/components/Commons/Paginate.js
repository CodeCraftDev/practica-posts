import React from 'react';
import {Box, Grid} from '@chakra-ui/core';
import ReactPaginate from "react-paginate";

export const Paginate = props => {

  const {total, limit, onPageChange} = props;

  return (
    <Box justifySelf='end'>
      {
        total &&
        <ReactPaginate
          previousLabel={'<'}
          nextLabel={'>'}
          breakLabel={'...'}
          breakClassName={'break-me'}
          pageCount={total / limit}
          marginPagesDisplayed={1}
          pageRangeDisplayed={1}
          onPageChange={onPageChange}
          containerClassName={'pagination'}
          subContainerClassName={'pages pagination'}
          activeClassName={'active'}
        />
      }
    </Box>
  )
};

