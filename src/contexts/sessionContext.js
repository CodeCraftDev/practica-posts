import React, { createContext, useEffect, useState } from 'react'
import redirectTo from "../helpers/redirectTo"

export const SessionContext = createContext()

export const SessionContextProvider = ({ children }) => {

  const [token, setToken] = useState('')
  const [session, setSession] = useState({})

  useEffect(() => {
    getSession()
  }, [])

  const getSession = async () => {
    const t = Cookies.get(process.env.TOKEN_KEY)
    await oauth.setToken(t)
    try {
      const res = await oauth.call('/session')
      if (res.code === 403) {
        throw new Error()
      }
      setToken(t)
      setSession(res)
    } catch (e) {
      redirectTo(`/`) // status: 301
    }
  }

  const logout = () => {
    oauth.call('/logout', { method: 'post' })
      .then(async () => {
        await Cookies.remove(`${process.env.TOKEN_KEY}`, { domain: process.env.MAIN_DOMAIN })
        await redirectTo(`/`)
      })
  }

  return <SessionContext.Provider
    value={{
      token,
      session,
      logout
    }}
  >
    {children}
  </SessionContext.Provider>
}
