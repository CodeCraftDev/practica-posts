import { Fragment } from 'react';
import { ToastProvider } from 'react-toast-notifications'
import Head from 'next/head';

const MainLayout = props => {

  return <ToastProvider>
    <Fragment>
      <Head>
        <title>{process.env.PAGE_TITLE || 'Site'}</title>
        <meta name='viewport' content='width=device-width, initial-scale=1'/>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500"
        />
        {/*Font awesome icons*/}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css"
              integrity="sha256-zmfNZmXoNWBMemUOo1XUGFfc0ihGGLYdgtJS3KCr/l0=" crossOrigin="anonymous"/>
      </Head>

      <div className="body-like">
        <div className="wrapper">
          {props.children}
        </div>
      </div>

    </Fragment>
  </ToastProvider>;
};

export default MainLayout;
