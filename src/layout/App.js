import React, { Fragment, useState } from 'react'
import Head from 'next/head'
import { ToastProvider } from 'react-toast-notifications'
import { Box, Breadcrumb, BreadcrumbItem, BreadcrumbLink, Grid } from '@chakra-ui/core'
import { Link } from './../../routes'
import { LayoutManager, modeGenerator, NavigationProvider, ThemeProvider, GlobalNavigationSkeleton } from '@atlaskit/navigation-next'
import { colors } from '@atlaskit/theme'
import { Nav } from './../components/Commons/Nav'

/**
 *
 * @param children
 * @param title
 * @param subtitle
 * @param Menu
 * @param isResizeDisabled: Boolean
 * @param hideAppSwitcher: Boolean
 * @returns {*}
 * @constructor
 */
export const AppLayout = ({ children, title, subtitle, Menu, isResizeDisabled, hideAppSwitcher }) => {

  const [collapse, setCollapsed] = useState(false)

  const homeLink = <Link route='dashboard'><a>Home</a></Link>

  const customThemeMode = modeGenerator({
    product: {
      text: colors.N900,
      background: colors.N0,
    },
  })

  const renderGlobalNavigation = () => {
    return <Nav hideAppSwitcher={hideAppSwitcher} />
  }

  const renderMenu = () => {
    return Menu || null
  }

  const renderChildren = () => (
    <Box m={4}>
      {children}
    </Box>
  )

  const renderLayout = () => {
    return (
      <Grid>
        <NavigationProvider initialUIController={{isCollapsed: false, isResizeDisabled}}>
          <ThemeProvider theme={theme => ({ ...theme, mode: customThemeMode })}>
            <LayoutManager
              showContextualNavigation={renderMenu()}
              globalNavigation={renderGlobalNavigation}
              productNavigation={renderMenu}
              onCollapseStart={() => setCollapsed(true)}
              onExpandStart={() => setCollapsed(false)}
              style={{ overflowY: 'hidden' }}
            >

            </LayoutManager>
          </ThemeProvider>
        </NavigationProvider>
      </Grid>
    )
  }

  return (
    <ToastProvider>
      <Fragment>
        <Head>
          <title>{title
            ? `${title} - ${process.env.PAGE_TITLE}`
            : process.env.PAGE_TITLE || 'Site'}</title>
          <meta name='viewport' content='width=device-width, initial-scale=1'/>
          {/*Font Family Roboto*/}
          <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet"/>
          <link href="/style/nprogress.css" rel="stylesheet"/>
          {/*Font awesome icons*/}
          <link rel="stylesheet"
                href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css"
                integrity="sha256-zmfNZmXoNWBMemUOo1XUGFfc0ihGGLYdgtJS3KCr/l0="
                crossOrigin="anonymous"
          />
        </Head>

        {
          renderChildren()
        }


      </Fragment>

      { /*language=CSS*/}
      <style jsx global>{`
        body {
          margin: 0;
          font-family: 'Lato', sans-serif;
          background-color: white;
        }

        .padding-container {
          padding: 15px;
        }

        .grid {
          display: grid;
          grid-template-columns: 3fr 1fr;
          grid-gap: 15px;
        }

        .grid-column-75-25 {
          display: grid;
          grid-template-columns: 3fr 1fr;
          grid-gap: 15px;
        }

        .grid-row-auto-auto {
          display: grid;
          grid-template-rows: auto auto;
          /*grid-gap: 15px;*/
        }

        .grid-50 {
          display: grid;
          grid-template-columns: 1fr 1fr;
          /*grid-gap: 15px;*/
        }

        .grid-1-auto {
          display: grid;
          grid-template-columns: 1fr auto;
          grid-gap: 15px;
        }

        .grid-auto-right {
          display: grid;
          grid-template-columns: 1fr auto;
          grid-gap: 15px;
        }

        .text-ellipsis {
          text-overflow: ellipsis;
          white-space: nowrap;
          overflow: hidden;
        }

        .justify-right {
          display: grid;
          justify-content: end;
        }

        .justify-center {
          display: grid;
          justify-content: center;
        }

        .justify-left {
          display: grid;
          justify-content: start;
        }
        
        .pagination {
        display: flex !important;
        padding: 0 !important;
        list-style: none !important;
      }

      .pagination li {
        min-width: 2.5rem;
        text-align: center;
        -webkit-transition: all 250ms;
        transition: all 250ms;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        height: 2.5rem;
        /*background-color: #fff;*/
      }

      .pagination li a:focus {
        outline: none;
      }

      .pagination li.active {
        background-color: #EDF2F7;
        padding: 0 10px;
        border-radius: 0.25rem;
        font-weight: 600;
        font-size: 1rem;
      }

      .pagination li.active:hover {
        background-color: #E2E8F0;
      }

      .pagination li.previous, li.next {
        /*background-color: #fff;*/
        color: #767676;
        font-size: 20px;
        font-weight: bold;
      }
      
      .no-focus {

      }

      .no-focus:focus {
        box-shadow: none !important;
        outline: none !important;
      }
      `}</style>

      { /*language=CSS*/}
      <style jsx>{`
        .app-content {
          height: calc(100vh - 56px);
          background-color: #fff;
        }
      `}</style>
    </ToastProvider>
  )
}

