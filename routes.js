const routes = require('next-routes');

// Name   Page      Pattern
module.exports = routes()                                   // ----   ----      -----
  .add('home', '/', 'index')
  .add('dashboard', '/dashboard', 'dashboard')
  // .add('about')                                             // about  about     /about
  // .add('channel', '/:slug.:id', 'channel')                         // blog   blog      /blog/:slug
  // .add('podcast', '/:slugChannel.:idChannel/:slug.:id', 'podcast');
  // .add('user', '/user/:id', 'profile')                // user   profile   /user/:id
  // .add('/:noname/:lang(en|es)/:wow+', 'complex')      // (none) complex   /:noname/:lang(en|es)/:wow+
  // .add({ name: 'beta', pattern: '/v3', page: 'v3' })        // beta   v3        /v3
