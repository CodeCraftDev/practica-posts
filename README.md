# ATENCION

Por un problema entre sass  y la version 9.0.7 se dee instalar next en la version 9.0.5 o no utilizar sass por ahora

# Frontend NextJs (Base)

Proyecto base para cualquier Frontend bajo el framework de NextJs

## Getting Started

Se debe clonar el repo e instalar las dependencias necesarias que ya están incluidas en `package.json`

### Prerequisites

Las librerias necesarias son:

```
"@zeit/next-sass": "^1.0.1" // Sass
"asteroid": "^2.0.3" // Para user de comet (backend meteor)
"crypto-js": "^3.1.9-1"
"dotenv": "^8.1.0"
"express": "^4.17.1"
"formik": "^1.5.8"
"isomorphic-unfetch": "^3.0.0"
"moment": "^2.24.0"
"next-routes": "^1.4.2"
"node-sass": "^4.12.0" // necesario para sass
"nprogress": "^0.2.0"
"react-toast-notifications": "^2.2.5" //toast
"slugify": "^1.3.5"
"universal-cookie": "^4.0.2"
```

Adicional los comandos de ejecución son:

```
"dev": "node server.js" // ejecución en local
"build": "next build"
"start": "NODE_ENV=production node server.js"
```

### Installing and Running

Para iniciar el proyecto de deben ejecutar los siguientes comandos


```
git clone https://username@bitbucket.org/CodeCraftDev/frontend-nextjs.git project-name
cd project-name
npm install
npm run dev
```

El proyecto va a iniciar en el puerto 4000

```
http://localhost:4000
```

### .env File

Primero se debe agregar en la configuracion `next.config.js`

```
const webpack = require('webpack');
const { parsed: localEnv } = require('dotenv').config();
const withSass = require('@zeit/next-sass');

module.exports = withSass({
  /* config options here */
  webpack: config => {
    config.plugins.push(new webpack.EnvironmentPlugin(localEnv));
    return config;
  }
});
```

El archivo quedaria asi

```
PORT=4000
METEOR_BACKEND=localhost:300
METEOR_SSL=false
OAUTH_URL=http://localhost:22344/api/v1
OAUTH_SECRET=12345abc
```

### Folders and files

Explain what these tests test and why

```
Give an example
```

## Versioning

Version Actual `0.1.0`
