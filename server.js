// server.js
const next = require('next');
const routes = require('./routes');
const app = next({dev: process.env.NODE_ENV !== 'production'});
const handler = routes.getRequestHandler(app);
const port = process.env.PORT || 3000;

// With express
const express = require('express');
app.prepare().then(() => {
  express().use(handler).listen(port, () => {
    console.info('\x1b[46m\x1b[30m%s\x1b[0m\x1b[0m', `Running on localhost:${port}`);
  });
});

// // Without express
// const {createServer} = require('http');
// app.prepare().then(() => {
//   createServer(handler).listen(3000)
// });
